# bestpracticesinpractice


There's a lot of "best practices" around, but after 19 years of work I've found a set that really helps get my systems done well. I'd like to share those tools and methods and why they work in my context.

## Description

There's a lot of "best practices" around, but after 19 years of work I've found a set that really helps get my systems done well. I'd like to share those tools and methods and why they work in my context. While they are not a novelty (some of them have years of history), I see even experienced developers ignoring them. These include: how to use BDD to work with requirements, using a scientific method to fix problems in production (as opposed to shotgun debugging), verifying observability during development (before going to production), making the most interesting parts visible via higher order functions, and more.
Nothing groundbreaking, but perhaps those will also work in your context.

---

Practices

0. Proving the validity: stop writing useless code
1. Using tests as a requirement management tool
2. Designing modules for cognitive load
- sharing the design with business
- MVP1 & MVP2
3. Implementing sculpture when you don't see the end
- 3D printing approach (you need to see the outcome)
- Negative scenarios: Minimal steps TDD
- Positive scenario: Moving Backwards
- Positive scenario: Up-front design and filling details
- Happy playthrough: learning by chaos, reimplementing
- Refactoring: writing with the reader in mind
-- extract method
-- higher order functions
-- explicite vs implicit
- Summary: bringing the solution by Approximation
4. Logging for observability & Metrics
5. Bug in production: shotgun debugging vs scientific approach
6. Mob code-review

---

TODO

Practices

+ Describe current situation, invoice importer
+ Describe new requirements, draw UI mockup

0. Proving the validity: stop writing useless code

Describe

1. Requirements & Design

+ Write down initial scenarios
+ Describe initial arch idea and why it's wrong
+ Draw modules, draw sequence diagra, describe 3 module design. Show lower cognitive load
+ Split requirements per module, describe MVP1 & MVP2
+ Show spock reports (Create reports, send to business)

- Using tests as a requirement management tool
- Spock Reports
- Redefining requirements per module
- Designing modules for cognitive load
- sharing the design with business
- MVP1 & MVP2

3. Implementing sculpture when you don't see the end
- 3D printing approach (you need to see the outcome)
- Negative scenarios: Minimal steps TDD
- Positive scenario: Moving Backwards
- Positive scenario: Up-front design and filling details
- Happy playthrough: learning by chaos, reimplementing
- Refactoring: writing with the reader in mind
-- extract method
-- higher order functions
-- explicite vs implicit
- Summary: bringing the solution by Approximation
4. Logging for observability & Metrics
5. Bug in production: shotgun debugging vs scientific approach
6. Mob code-review




--------

workspace "practices in practice" "Best Practices in practice talk" {

    model {
        # userSnapshotTopic = kafkaTopic "Users snapshot in Kafka"
    
        customer = person "Backoffice user" "Backoffice user" "Customer"
        enterprise "Big Bank plc" {
            partner = softwaresystem "Partner" "External Partner" "Existing System"
            billing = softwaresystem "Billing" "Does the billing. Off the shelf product"
            internetBankingSystem = softwaresystem "Internet Banking System" "Allows customers to view information about their bank accounts, and make payments." {
                invoiceImporterMs = container "Invoice importer" "Microservice importing and validating invoices by a very peculiar protocol" {
                    invoiceImporter = component "Invoice importer" "Validates invoice, enriches with user, partner and service data, converts to billing invoice"
                }
            
            }
        }

        partner -> invoiceImporter "invoice issued" "Peculiar protocol"
        invoiceImporter -> billing "invoice issued by partner"

        element customerSnapshot "Kafka Topic" "Our customers" {
            -> invoiceImporter "UserSnapshot" "Kafka event"
        }

        element partnerCredentials  "Kafka Topic" "Credentials for partners we have agreements with" {
            -> invoiceImporter "PartnerCredentials" "Kafka event"
        }
        
        element partnerDeviceSnapshot  "Kafka Topic" "Partner's devices our users can use" {
            -> invoiceImporter "partnerDeviceSnapshot" "Kafka event"
        }
    
    }

    views {
        systemcontext internetBankingSystem "SystemContext" {
            include *
            animation {
            }
            autoLayout
        }

        container internetBankingSystem "Containers" {
            include *
            animation {

            }
            autoLayout
        }

        component invoiceImporterMs "Components" {
            include *
            animation {
                invoiceImporter
            }
            autoLayout
        }

        styles {
            element "Person" {
                color #ffffff
                fontSize 22
                shape Person
            }
            element "Customer" {
                background #08427b
            }
            element "Bank Staff" {
                background #999999
            }
            element "Software System" {
                background #1168bd
                color #ffffff
            }
            element "Existing System" {
                background #999999
                color #ffffff
            }
            element "Container" {
                background #438dd5
                color #ffffff
            }
            element "Web Browser" {
                shape WebBrowser
            }
            element "Mobile App" {
                shape MobileDeviceLandscape
            }
            element "Database" {
                shape Cylinder
            }
            element "Component" {
                background #85bbf0
                color #000000
            }
            element "Failover" {
                opacity 25
            }
        }
    }
}


---

workspace "practices in practice" "Best Practices in practice talk" {

    model {
        # userSnapshotTopic = kafkaTopic "Users snapshot in Kafka"
    
        backofficeUser = person "Backoffice user" "Backoffice user" "Customer"
        enterprise "Big Bank plc" {
            partner = softwaresystem "Partner" "External Partner" "Existing System"
            billing = softwaresystem "Billing" "Does the billing. Off the shelf product"
            internetBankingSystem = softwaresystem "Part of our microservice based system" "" {
                invoiceImporterMs = container "Invoice importer" "Microservice importing and validating invoices by a very peculiar protocol" {
                    invoiceImporter = component "Invoice importer" "Validates invoice, enriches with user, partner and service data, converts to billing invoice"
                    xlsxUploader = component "XLSX Uploader" "REST file upload & XLSX parsing"
                    reporter = component "Import Reporter" "Reports import outcomes"
                }
            
            }
        }

        partner -> invoiceImporter "invoice issued" "Peculiar protocol"
        invoiceImporter -> billing "invoiceIssuedByPartner" "Kafka event"
        backofficeUser -> xlsxUploader "import invoice" "HTTP file upload"
        xlsxUploader -> invoiceImporter "invoice issued" "Facade method call"
        xlsxUploader -> reporter "import finished" "Facade method call"
        backofficeUser -> reporter "get last N import outcomes" "HTTP GET"

        element customerSnapshot "Kafka Compacted Topic" "Our customers" {
            -> invoiceImporter "UserSnapshot" "Kafka event"
        }

        element partnerCredentials  "Kafka Compacted Topic" "Credentials for partners we have agreements with" {
            -> invoiceImporter "PartnerCredentials" "Kafka event"
        }
        
        element partnerDeviceSnapshot  "Kafka Compacted Topic" "Partner's devices our users can use" {
            -> invoiceImporter "partnerDeviceSnapshot" "Kafka event"
        }
    
    }

    views {
        systemcontext internetBankingSystem "SystemContext" {
            include *
            animation {
            }
            autoLayout
        }

        container internetBankingSystem "Containers" {
            include *
            animation {

            }
            autoLayout
        }

        component invoiceImporterMs "Components" {
            include *
            animation {
                invoiceImporter
            }
        }

        styles {
            element "Person" {
                color #ffffff
                fontSize 22
                shape Person
            }
            element "Customer" {
                background #08427b
            }
            element "Bank Staff" {
                background #999999
            }
            element "Software System" {
                background #1168bd
                color #ffffff
            }
            element "Existing System" {
                background #999999
                color #ffffff
            }
            element "Container" {
                background #438dd5
                color #ffffff
            }
            element "Web Browser" {
                shape WebBrowser
            }
            element "Mobile App" {
                shape MobileDeviceLandscape
            }
            element "Database" {
                shape Cylinder
            }
            element "Component" {
                background #85bbf0
                color #000000
            }
            element "Failover" {
                opacity 25
            }
        }
    }
}


---

workspace "practices in practice" "Best Practices in practice talk" {

    model {
        # userSnapshotTopic = kafkaTopic "Users snapshot in Kafka"
    
        backofficeUser = person "Backoffice user" "Backoffice user" "Customer"
        enterprise "Big Bank plc" {
            partner = softwaresystem "Partner" "External Partner" "Existing System"
            billing = softwaresystem "Billing" "Does the billing. Off the shelf product"
            internetBankingSystem = softwaresystem "Part of our microservice based system" "" {
            
                invoiceImporter = container "Invoice importer" "Validates invoice, enriches with user, partner and service data, converts to billing invoice"
                xlsxUploader = container "XLSX Uploader" "REST file upload & XLSX parsing"
                reporter = container "Import Reporter" "Reports import outcomes"
            
            
            }
        }

        partner -> invoiceImporter "invoice issued" "Peculiar protocol"
        invoiceImporter -> billing "invoiceIssuedByPartner" "Kafka event"
        backofficeUser -> xlsxUploader "import invoice" "HTTP file upload"
        xlsxUploader -> invoiceImporter "invoice issued" "Kafka event"
        xlsxUploader -> reporter "import finished" "Kafka event"
        backofficeUser -> reporter "get last N import outcomes" "HTTP GET"

        element customerSnapshot "Kafka Compacted Topic" "Our customers" {
            -> invoiceImporter "UserSnapshot" "Kafka event"
        }

        element partnerCredentials  "Kafka Compacted Topic" "Credentials for partners we have agreements with" {
            -> invoiceImporter "PartnerCredentials" "Kafka event"
        }
        
        element partnerDeviceSnapshot  "Kafka Compacted Topic" "Partner's devices our users can use" {
            -> invoiceImporter "partnerDeviceSnapshot" "Kafka event"
        }
    
    }

    views {
        systemcontext internetBankingSystem "SystemContext" {
            include *
            animation {
            }
            autoLayout
        }

        container internetBankingSystem "Containers" {
            include *
            animation {
            }
        }

        styles {
            element "Person" {
                color #ffffff
                fontSize 22
                shape Person
            }
            element "Customer" {
                background #08427b
            }
            element "Bank Staff" {
                background #999999
            }
            element "Software System" {
                background #1168bd
                color #ffffff
            }
            element "Existing System" {
                background #999999
                color #ffffff
            }
            element "Container" {
                background #438dd5
                color #ffffff
            }
            element "Web Browser" {
                shape WebBrowser
            }
            element "Mobile App" {
                shape MobileDeviceLandscape
            }
            element "Database" {
                shape Cylinder
            }
            element "Component" {
                background #85bbf0
                color #000000
            }
            element "Failover" {
                opacity 25
            }
        }
    }
}



----

# Refine MVP1 requirements

```java
def "should convert each line except header into InvoiceEvent, 
     pass to invoice import and reporting"() {
   given: "there is an xlsx file with two lines"
       File file = createFileWithInvoices(invoiceRow1, invoiceRow2)

   when: "file is uploaded"
       UploadResult result = xlsxUploadFacade.importFromFile(file, userName)

   then: "two were successfully imported"
       result.successfullyImported() == 2

   and: "the result contains file name"
       result.fileName() == fileName

   and: "the result contains user name"
       result.userName() == userName

   and: "module calls importer twice with two InvoiceEvents"
       1 * importerFacade.importInvoice(eventFor(invoiceRow1)) >> success() 
       1 * importerFacade.importInvoice(eventFor(invoiceRow2)) >> success() 

   and: "import reporter was called once with the result"
       1 * reporterFacade.report(_ as UploadResult)
}
```

---

```java
def "loading data from #file.name results in error #expectedError"() {
   when: "file is uploaded"
       UploadResult result = xlsxUploadFacade.importFromFile(file, userName)

   then: "module returns error"
       result.failedWith(expectedError)

   and: "summary is not sent to manual import reporter"
       0 * reporterFacade.report(_ as UploadResult)

   where:
       file                          || expectedError
       loadFile("not-xlsx-file.txt") || FILE_CANNOT_BE_READ
       loadFile("empty-xlsx.xlsx")   || FILE_IS_EMPTY
       new File("not-existing-file") || FILE_CANNOT_BE_READ
       loadFile("encrypted.xlsx")    || FILE_IS_ENCRYPTED
}
```

---

```java
def "when cell cannot be processed, should return error, 
     and send the whole row as csv to reporter"() {

   given: 'there is an xlsx file with error in a cell in row 2'
       File file = createFileWithInvoices(invoiceRow1, invalidRow)

   and: "importer correctly does its job on row 1"
       importerFacade.importInvoice(eventFor(invoiceRow1)) >> Results.success()

   when: "file is uploaded"
       UploadResult result = xlsxUploadFacade.importFromFile(file, userName)

   then: "module sends error message with original row to import reporter"
       1 * reporterFacade.report(
            {it.originalRowValues() == asCsv(invalidRow)} as UploadResult)

   and: "the result has error for that row"
       result.failedWith(ROW_NOT_CONVERTABLE)
}
```

---

```java
def "error from importer means the row is not imported correctly"() {
   given: "there is an xlsx file with a line"
       File file = createFileWithInvoices(invoiceRow1, invoiceRow2)

   and: "importer fails on first import but is ok for second"
       importerFacade.importInvoice(_ as RoamingCdrImportedEvent) >>> 
            [Results.failure("dupa"), Results.success()]

   when: "file is uploaded"
       UploadResult result = xlsxUploadFacade.importFromFile(file, userName)

   then: "only one was successfully imported"
       result.successfullyImported() == 1

   and: "one row was reported as error in importer"
       result.failedWith(IMPORT_MODULE_REJECTED)

   and: "import reporter was called once with the result"
       1 * reporterFacade.report(
            {it.originalRowValues() == asCsv(invalidRow)} as UploadResult)
}
```

```java
def "for #error the message sent to reporter should be #errorMessage"() {
   given: "there is an xlsx file with error in a field"
        ...
   when: "file is uploaded"
        ...
   then: "module sends error message with original row to import reporter"
        ...
}
```
