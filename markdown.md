name: inverse
layout: true
class: inverse

---
template: inverse
background-image: url(img/ctulhu.png)
background-size: 100%
background-repeat: no-repeat
background-position: center top
class: bottom middle

#### Best practices in practice 

#### things that work for me so well I cannot believe you are not using them

by Jakub Nabrdalik

---
layout: false
background-image: url(img/iga.gif)
background-size: 100%
background-repeat: no-repeat
background-position: center top
class: bottom middle

#### Best practices in practice 

#### things that work for me so well I cannot believe you are not using them

by Jakub Nabrdalik

---

template: inverse
background-image: url(img/Horse_and_cars_side_by_side_2.jpeg)
background-size: 100%
background-repeat: no-repeat
background-position: center top
class: bottom right

### “The future is already here – it's just not evenly distributed."

#### William Gibson, The Economist, December 4, 2003

---

layout: false
background-image: url(img/caveat-emptor.jpeg)
background-size: 120%
background-repeat: no-repeat
background-position: center middle
class: bottom middle

???

Practices are best only in specific context, what works for me, may not work for you

---

## My context

DevOps - designing, developing and operating my shit on production (9 years)

Previously software houses - building shit for others (10 years)

7 years in microservices, 12 in monoliths (including modular and non modular)

Large enterprises (20k+ people) and small startups (9 people)

12+ years consulting, lecturing, giving workshops, and generally working with teams from different companies


---

## Situational context

A medium sized event-driven microservice based system (say, 10 teams), kubernetes, kafka, public cloud, all the jazz

Complex domain (users, devices, services from many companies, cross charging)

Microservice in question: Invoice importer.

???

Everything I’m going to tell you is based on real situations and practices I use, but to protect my client I have changed small details here and there.

The situation is like this: there is a medium sized event-driven microservice based system (say, 10 teams) that talks with other companies when needed. The business domain is very complex: we have our users, devices, processes, our clients have subscription agreements with us but use devices of other services, at the end we are charged, and need to charge our clients.

---

layout: false
background-image: url(img/importer-initial.png)
background-size: 95%
background-repeat: no-repeat
background-position: center middle
class: center top

## Current architecture

???

There is a particular microservice responsible for importing invoices by a very peculiar protocol. It takes an invoice, validates the partner, our user, the services used using several read models from Kafka, and if everything is ok, converts that to something our billing microservice can handle and passes it there as a Kafka event.

Responsible for: peculiar HTTP based protocol, protocol-centric, partner-centric validation, convertion to Kafka event for Billing.



---

## New requirement

The product owner joins us at the planning session.

So what are the requirements?

```
I want to upload the excel file so I do not need to perform all the checks, 
validations and enter the data myself into the billing system
```

???

There comes a new requirement: our clients sometimes need to create a corrective invoice, and due to their stupid systems, they cannot send those over to us using the same protocol we use for everything, but instead they send it via email, and we need to upload them manually.

For now it means the business needs to do all the checks and conversions by hand, and then enter it into billing. The product owner would like to automate that.

---

## Prove validity: stop writing useless code

Can you prove that feature is actually worth implementing?

--

How many invoices per month will be uploaded this way? 
> Does it even make sense to automate?

--

What data variation can we expect in the invoice format in the future?

> Is there a contract for data?

> What will be the maintenance cost?

--

What is the current time/cost for uploading them the old way?

> this vs dev salary (implementation + maintenance) for the next 3 years

???

(we are trying to keep the maintenance low, we cannot allow ourselves to create a high maintenance feature with so many microservices)

If we don’t know the answer how about a pretotype: upload xlsx, limit to 100, manually add them to billing first?

I had cases when a PO would promise something to someone or have a feature in his/her performance goals, and therefore would push for something that won’t even be used in prod.

On the other hand just because a feature won’t be used, doesn’t mean there is no benefit: Allegro DE 7 years ago - lot’s of money earned, nothing used by end users.

---

## Proving the validity: analysis

Processing manually = 40k Euro / month

--

*Pros*

> Around 20k euro per month saved on business operations.

> Trust of billing business people increased.

> Political gains ???

--

*Cons*

> 10k euro per man month of implementation 

> Maintenance cost + infra cost 250 euro per month

> Opportunity cost ???

???

Ok, so it seems we have some numbers. Processing those invoices manually costs us 40k euro per month. Assuming 35k PLN for man-month of a senior in Warsaw, the balance looks like this:

The feature looks economical, assuming we can do it in 2 weeks or so, and there’s nothing better in the backlog (opportunity cost). PO can confirm this.

Let’s analyse it.

---

layout: false
background-image: url(img/UImockup.png)
background-size: 70%
background-repeat: no-repeat
background-position: center bottom
class: left top

## Requirements are not analysis

```
I want to upload the excel file so I do not need to perform all the checks, 
validations and enter the data myself into the billing system
```

---

## Refine requirements: write them down

```
Positive scenarios (all good):
should convert each line into an invoice and pass to billing
should return list of imports

Positive scenarios (some errors in lines):
should return a list of errors for an import
should download a csv file with lines from the imported files that contain an error
many error in a single line should all be reported
for error the message returned should be errorMessage

Corner cases:
only accept files smaller than 100MB
only logged users can submit files
should fail on files which are not xlsx
should fail on empty file
```

???

I've seen teams trying to implement a user story. User story does not describe requirements. It's an invitation to conversation. Someone has to think through all the interactions of the the system, and your business should at least verify it. 

Ways to do it:

Examples (xlsx files, how its visible in billing later on)
More examples (errors in xlsx with description)
Behavioral Scenarios (how the system behaves - all the interactions with the system)

---

layout: false
background-image: url(img/importer-initial.png)
background-size: 95%
background-repeat: no-repeat
background-position: center middle
class: left top

## Designing the architecture

???

Let’s design the architecture.

“What’s to design? We just need to add this feature to the import microservice!”

Let’s consider what will happen.

---

## Add feature to existing module

Current cognitive load
- how to build read models from Kafka streams
- how to send events to kafka
- how to validate partner, user and services billed for
- how invoices are created and represented on both sides
- peculiar protocol we have with the partner

--

Additional cognitive load
- file upload/download via Spring MVC
- how to read xlsx files (Apache POI)
- how to create a CSV file based on errors
- how to store/retrieve data from DB, migrate schema, access vault in pipeline

???

Currently this microservice has one module, responsible for importing an invoice. 
To be able to work with it you need to know (apart from basic java/k8s knowledge)
how to build read models from Kafka streams
how to send events to kafka
how to validate partner, user and services billed for
how invoices are created and represented on both sides
peculiar protocol we have with the partner

If we add this as a feature inside the current module, you will also have to know
file upload/download via Spring MVC
how to read xlsx files (Apache POI)
how to create a CSV file based on errors
how to store/retrieve data from DB, migrate schema, access vault in pipeline

The cognitive load on the person was already high, but now it will be twice the size, so it will be twice as hard to maintain. Not good.

---

## Add feature as separate modules

---

layout: false
background-image: url(img/importer-3modules.png)
background-size: 70%
background-repeat: no-repeat
background-position: center middle
class: center top

???

xlsx upload module (upload and xlsx processing/conversion)
feeding invoice importing module
feeding xlsx upload reporting module

---

layout: false
background-image: url(img/importer-3modules-tall.png)
background-size: 45%
background-repeat: no-repeat
background-position: center right

## Separate modules: cognitive load

.left-column[

invoice importing module
> no changes

xlsx uploader module
> file upload/download via Spring MVC;
> how to read xlsx files (Apache POI)

reporter module
> how to store/retrieve data from DB, migrate schema, access vault in pipeline;
> how to create a CSV file based on errors

Much lower cognitive load per module.

Lower implementation risk.

]

---

## Design: How should it fail?

Identify what can go wrong

Identify countermeasures or manage risk

[https://owasp.org/www-community/Threat_Modeling](https://owasp.org/www-community/Threat_Modeling)

[https://owasp.org/www-community/Threat_Modeling_Process](https://owasp.org/www-community/Threat_Modeling_Process)

--

#### In our case: 

Attack/Failure surface increases

> UI endpoints

> bugs in Apache POI

> bugs in parsing

> memory leaks

???

There is one more thing to consider: security. 
Adding a module increases the attack surface. Now we have to consider UI endpoints, bugs in Apache POI, and bugs in parsing. Looks like a lot that could go wrong in here. We could do a full threat modeling

Yet, out PO is ok with this threat increase for a while, because he assumes, we are likely going to extract it later on.

---

## Add feature as separate microservices

---

layout: false
background-image: url(img/importer-3microservices.png)
background-size: 70%
background-repeat: no-repeat
background-position: center middle
class: center top

---

## Add feature as separate microservices

Lower attack surface

Lower failure impact

Safer implementation

--

#### Different quality requirements

> Invoice Importer: SLA 99%, RPS 10, FoC: Low

> XLSX Uploader and Import Reporter: SLA 0%, RPS 0, FoC: ??

--

One more service to monitor

Memory consumption may not be worth it (Scaling to 0? Serverless?)

--

*We chose the cheapest option that we can get the data from and easily refactor*

???

What if we make those two by building those two additional modules as separate microservices communicating via Kafka?. This will lower the failure domain and risk. Even if we fail completely (out of memory, etc) existing processing will not be impacted. However, we don’t know yet, what are the usage peaks: keeping a full pod on k8s for 24/7 makes little sense if it’s only used for 2h a day. Then we could go serverless, but that’s expensive and it doesn’t like long processing. 

So the conclusion is: adding it to existing module is a bad idea. Whether a separate microservice is going to be economical, we don’t know yet. Therefore, let’s choose the cheapest option that we can get the data from and then easily refactor. That’s two additional modules in existing microservice scenario.

---

layout: false
background-image: url(img/importer-3modules-tall.png)
background-size: 45%
background-repeat: no-repeat
background-position: center right

## Always create MVPs

<!-- TODO: add UI example with returned outcome -->

--

*MVP1 xlsx upload module*
> should convert each line into an invoice, <br/> pass to invoice import and reporting

> only accept files smaller than 100MB

> only logged users can submit files

> should fail on files which are not xlsx

> should fail on empty file

> many error in a single line should all be reported

> for error the message returned should be errorMessage

*MVP2 reporting module*
> should return list of imports

> should return a list of errors for an import

> should download a csv file with lines containing an error

???

Let’s split requirements and the design it into MVPs

This allows us to deliver business value earlier and get earlier feedback.

Let’s refine requirements for MVP1 and send them + design to business for feedback.

---

## Refine MVP1 requirements

```java
def "should convert each line except header into InvoiceEvent, 
     pass to invoice import and reporting"() {
   given: "there is an xlsx file with two lines"
   when: "file is uploaded"
   then: "two were successfully imported"
   and: "the result contains file name"
   and: "the result contains user name"
   and: "module calls importer twice with two InvoiceEvents"
   and: "import reporter was called once with the result"
}
```

```java
def "loading data from #file.name results in error #expectedError"() {
   when: "file is uploaded"
   then: "module returns error"
   and: "summary is not sent to manual import reporter"
   where:
       file                          || expectedError
       loadFile("not-xlsx-file.txt") || FILE_CANNOT_BE_READ
       loadFile("empty-xlsx.xlsx")   || FILE_IS_EMPTY
       new File("not-existing-file") || FILE_CANNOT_BE_READ
       loadFile("encrypted.xlsx")    || FILE_IS_ENCRYPTED
}
```

---

```java
def "when cell cannot be processed, should return error, 
     and send the whole row as csv to reporter"() {
   given: 'there is an xlsx file with error in a cell in row 2'
   and: "importer correctly does its job on row 1"
   when: "file is uploaded"
   then: "module sends error message with original row to import reporter"
   and: "the result has error for that row"
}
```

```java
def "error from importer means the row is not imported correctly"() {
   given: "there is an xlsx file with a line"
   and: "importer fails on first import but is ok for second"
   when: "file is uploaded"
   then: "only one was successfully imported"
   and: "one row was reported as error in importer"
   and: "import reporter was called once with the result"
}
```

```java
def "for #error the message sent to reporter should be #errorMessage"() {
   given: "there is an xlsx file with error in a field"
   when: "file is uploaded"
   then: "module sends error message with original row to import reporter"
   where: "..." //we need more examples for that
}
```

---

What we don’t need anymore?

*only accept files smaller than 100MB*

> because Apache POI allows working on filesystem without loading the whole file into memory

*only logged users can submit files*

> because in this app nothing is possible for not logged files


---

layout: false
background-image: url(img/spockreports.png)
background-size: 50%
background-repeat: no-repeat
background-position: center right
class: left top

.left-column[

## Use tests as a requirement management tool

Create all those tests in a branch

Run Spock Reports

Send report (test scenarios) to the business for verification

Ask the business for examples of errors (for the last scenario)

]

---

## Best practices before we write the code

Prove the validity: stop writing useless code

Design the architecture

> optimize modules for minimum cognitive load

> check different quality requirements (frequency of change, availability, traffic)

> look at alternatives, do not fixate on first solution

Design for failure (threat modelling)

Split solution into MVPs (early feedback is crucial)

Refine requirements as Behavioral Scenarios for each module (analysis)

Use tests to manage requirements, use them to communicate with business

--

*All of this is doable in 1h*

---

layout: false
background-image: url(img/underextrusion-gif.gif)
background-size: 50%
background-repeat: no-repeat
background-position: center middle
class: center top

## Implementing practices when you don't see the end

Sometimes you can see the whole solution and just print it out on the keyboard

???

Sometimes when we have a feature to implement, it's simple enough or we've done it many times before, so that now we see the whole solution with our minds eye, and we can just write it down like a printer, pixel by pixel, line by line.

Other times, we don't even know how to start. And here I see junior developers being hold back by fear, wasting time. And I see senior devs producing horrible things even by their own standards.

Let's talk about some practices that can help you solve this problem, unlock your creativity, and speed you up.

So what do we do when we don't know how to start?

---

layout: false
background-image: url(img/hammer.png)
background-size: 40%
background-repeat: no-repeat
background-position: center middle
class: center top

## You don't know how to do it but you are a senior

---

layout: false
background-image: url(img/panic.gif)
background-size: 40%
background-repeat: no-repeat
background-position: center middle
class: center top

## You don't know how to do it but you are a junior

---

## Baby steps TDD 

Split testing scenarios into trivial steps (or start with negative scenarios).

Implement a trivial step

Refactor so it looks good (for this step)

Move to the next step

(Sandro Mancuso has great examples of this approach)

---

```java
def "should convert each line except header into InvoiceEvent, 
     pass to invoice import and reporting"() {
   given: "there is an xlsx file with two lines"
   when: "file is uploaded"
   then: "two were successfully imported"
   and: "the result contains file name"
   and: "the result contains user name"
   and: "module calls importer twice with two InvoiceEvents"
   and: "import reporter was called once with the result"
}
```

---

```java
def setup() {
    File file = createFileWithInvoices(invoiceRow1, invoiceRow2)
}

def "should pass Upload Result to reporter"() {
    when: "file is uploaded"
        UploadResult result = xlsxUploadFacade.importFromFile(file, userName)

    then: "import reporter was called once with the result"
        1 * reporterFacade.report(result)
}
```

--

```java
@RequiredArgsConstructor
public class XlsxUploadFacade {
    private final ReporterFacade reporterFacade;

    public UploadResult importFromFile(File file, String username) {
        UploadResult result = new SuccessfulUploadResult();
        reporterFacade.report(result);
        return result;
    }
}
```


---

```java
def "should return imported file name"() {
    when: "file is uploaded"
        UploadResult result = xlsxUploadFacade.importFromFile(file, userName)

    then: "the result contains file name"
        result.fileName() == file.name
}
```

--

```java
@RequiredArgsConstructor
public class XlsxUploadFacade {
    private final ReporterFacade reporterFacade;

    public UploadResult importFromFile(File file, String username) {
        String fileName = file.getName();
        UploadResult result = new SuccessfulUploadResult(fileName);
        reporterFacade.report(result);
        return result;
    }
}
```

---

```java
def "should return username of the user that performed the import"() {
    when: "file is uploaded"
        UploadResult result = xlsxUploadFacade.importFromFile(file, userName)

    then: "the result contains user name"
        result.userName() == userName
}
```

--

```java
@RequiredArgsConstructor
public class XlsxUploadFacade {
    private final ReporterFacade reporterFacade;

    public UploadResult importFromFile(File file, String username) {
        String fileName = file.getName();
        UploadResult result = new SuccessfulUploadResult(fileName, username);
        reporterFacade.report(result);
        return result;
    }
}
```

Baby steps all the time!!!

---

Now we need to read rows from the file

```java
def "should return number of imported invoices"() {
    when: "file is uploaded"
        UploadResult result = xlsxUploadFacade.importFromFile(file, userName)

    then: "two were successfully imported"
        result.successfullyImported() == 2
}
```

--

Now we need to to read cells and build the event

```java
def "should call import module with invoice events"() {
    when: "file is uploaded"
        xlsxUploadFacade.importFromFile(file, userName)

    then: "module calls importer twice with two InvoiceEvents"
        1 * importerFacade.importInvoice(eventFor(invoiceRow1)) >> ImportResults.success()
        1 * importerFacade.importInvoice(eventFor(invoiceRow2)) >> ImportResults.success()
}
```

But this is difficult, let's find something simpler

---

Handling different file problems is easier than reading cells and building events, so let's do this instead

```java
def "loading data from #file.name results in error #expectedError"() {
        when: "file is uploaded"
            UploadResult result = xlsxUploadFacade.importFromFile(file, userName)

        then: "module returns error"
            result.failedWith(expectedError)

        and: "summary is not sent to manual import reporter"
            0 * reporterFacade.report(_ as UploadResult)

        where:
            file                           || expectedError
            loadFile("not-xlsx-file.txt")  || FILE_CANNOT_BE_READ
            loadFile("empty-xlsx.xlsx")    || FILE_IS_EMPTY
            new File("not-existing-file")  || FILE_CANNOT_BE_READ
            loadFile("encrypted.xlsx")     || FILE_IS_ENCRYPTED
    }
```

---

## Baby steps TDD

Good if you are completely stuck

Can be infuriating (when you already see a few moves ahead)

Makes you always move at a steady pace (albeit slow: lots of iterations)

Note - In this particular case, we'll need to merge all those small test back into a large one, because xlsx test is a slow/expensive one - you touch the IO on each scenario

```java
def "should convert each line except header into InvoiceEvent, 
     pass to invoice import and reporting"() {
   given: "there is an xlsx file with two lines"
   when: "file is uploaded"
   then: "two were successfully imported"
   and: "the result contains file name"
   and: "the result contains user name"
   and: "module calls importer twice with two InvoiceEvents"
   and: "import reporter was called once with the result"
}
```

---

## Backward programming

Start with a positive scenario

Start with returning the outcome (last line)

Search for what you need to build the outcome

Write from the end, towards the beginning of the method

---

```java
def "should convert each line except header into InvoiceEvent, 
     pass to invoice import and reporting"() {
    given: "there is an xlsx file with two lines"
        File file = createFileWithInvoices(invoiceRow1, invoiceRow2)

    when: "file is uploaded"
        UploadResult result = xlsxUploadFacade.importFromFile(file, userName)

    then: "two were successfully imported"
        result.successfullyImported() == 2

    and: "the result contains file name"
        result.fileName() == file.name

    and: "the result contains user name"
        result.userName() == userName

    and: "module calls importer twice with two InvoiceEvents"
        1 * importerFacade.importInvoice(eventFor(invoiceRow1)) >> success()
        1 * importerFacade.importInvoice(eventFor(invoiceRow2)) >> success()

    and: "import reporter was called once with the result"
        1 * reporterFacade.report(_ as UploadResult)
}
```

---

```java
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class XlsxUploadFacade {
    ReporterFacade reporterFacade;

    public UploadResult importFromFile(File file, String username) {
        ???
        sendToReporterIfAnySuccessful(result);
        return result;
    }
}
```

---

```java
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class XlsxUploadFacade {
    ReporterFacade reporterFacade;

    public UploadResult importFromFile(File file, String username) {
        ???
        sendToReporterIfAnySuccessful(result);
        return result;
    }

    private void sendToReporterIfAnySuccessful(UploadResult result) {
        if (result.successfullyImported() > 0) {
            reporterFacade.report(result);
        }
    }
}
```

---

```java
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class XlsxUploadFacade {
    ReporterFacade reporterFacade;

    public UploadResult importFromFile(File file, String username) {
        ???
        UploadResult result = createResult(successfullyImported, failedRows, 
            fileName, userName);
        sendToReporterIfAnySuccessful(result);
        return result;
    }
}
```
---

```java
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class XlsxUploadFacade {
    ReporterFacade reporterFacade;

    public UploadResult importFromFile(File file, String username) {
        ???
        UploadResult result = createResult(successfullyImported, failedRows, 
            fileName, userName);
        sendToReporterIfAnySuccessful(result);
        return result;
    }

    private UploadResult createResult(
            int successfullyImported, 
            List<ErrorAtRow> failedRows, 
            String fileName, 
            String userName) {
        return (failedRows.isEmpty())
                ? successOrNoRows(successfullyImported, fileName, userName)
                : failureOnRows(successfullyImported, failedRows, fileName, userName);
    }
}
```

---

```java
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class XlsxUploadFacade {
    ReporterFacade reporterFacade;

    public UploadResult importFromFile(File file, String username) {
        ???
        UploadResult result = createResult(successfullyImported, failedRows, 
            fileName, userName);
        sendToReporterIfAnySuccessful(result);
        return result;
    }

    private UploadResult createResult(
            int successfullyImported, 
            List<ErrorAtRow> failedRows, 
            String fileName, 
            String userName) {
        return (failedRows.isEmpty())
                ? successOrNoRows(successfullyImported, fileName, userName)
                : failureOnRows(successfullyImported, failedRows, 
                    fileName, userName);
    }

    private UploadResult successOrNoRows(
            int successfullyImported, 
            String fileName, 
            String userName) {
        return (successfullyImported == 0)
                ? failedOnFile(UploadResultFailure.FILE_IS_EMPTY, 
                    fileName, userName)
                : success(successfullyImported, fileName, userName);
    }
}
```


---

```java
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class XlsxUploadFacade {
    ReporterFacade reporterFacade;

    public UploadResult importFromFile(File file, String username) {
        ???        
        ConverterRows convertedRows = convertAndExport(sheet);
        int successfullyImported = countSuccessfullyImported(convertedRows);
        List<ErrorAtRow> failedRows = getErrorsAtRows(convertedRows);
        UploadResult result = createResult(successfullyImported, failedRows, 
            fileName, userName);
        sendToReporterIfAnySuccessful(result);
        return result;
    }
}
```

---

```java
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class XlsxUploadFacade {
    ReporterFacade reporterFacade;

    public UploadResult importFromFile(File file, String username) {
        ???
        Sheet sheet = workbook.getSheetAt(0);
        ConverterRows convertedRows = convertAndExport(sheet);
        int successfullyImported = countSuccessfullyImported(convertedRows);
        List<ErrorAtRow> failedRows = getErrorsAtRows(convertedRows);
        UploadResult result = createResult(successfullyImported, failedRows, 
            fileName, userName);
        sendToReporterIfAnySuccessful(result);
        return result;
    }
}
```

---

## Backward programming

Focuses you on what you need, not what you think you need

Makes it easy/natural to delegate work to other methods/objects

Main success scenario will not get implemented till you are finished, but corner cases will in the meantime

You can get stuck in the middle

---

## High level design and filling details

Explain on the top level the algorithm with delegation

Try to implement that

--

```java
    public UploadResult importFromFile(File file, String username) {
        Workbook workbook = WorkbookFactory.create(file);
        Sheet sheet = workbook.getSheetAt(0);
        ConverterRows convertedRows = convert(sheet);
        ExportedRows exportedRows = export(convertedRows);
        UploadResult result = 
            createResult(convertedRows, exporterRows, file, username);
        sendToReporterIfAnySuccessful(result);
        return result;
    }
```

--

The problem is, you've just ignored ugly reality

> Workbook creation throws a lot of non-runtime Exceptions

> You have to convert and export in a streaming style (not to load everything into memory)

> Use Flux (reactive programming) to do it efficiently

---

## Learning by chaos

Just play with it

```java
public UploadResult importFromFile(File file, String username) {
    try (Workbook workdupa = WorkbookFactory.create(file)) {
        log.info("work", workdupa);

        return null;
    } catch (Throwable t) {
        log.info("Niebangla", t);
        return null;
    }
}
```

---

## Learning by chaos

Just play with it

```java
public UploadResult importFromFile(File file, String username) {
    try (Workbook workdupa = WorkbookFactory.create(file)) {
        log.info("work", workdupa);
        var shiieeet = workdupa.getSheetAt(0);
        log.info("shit", shiieeet);

        return null;
    } catch (Throwable t) {
        log.info("Niebangla", t);
        return null;
    }
}
```

---

## Learning by chaos

Just play with it

```java
public UploadResult importFromFile(File file, String username) {
    try (Workbook workdupa = WorkbookFactory.create(file)) {
        log.info("work", workdupa);
        var shiieeet = workdupa.getSheetAt(0);
        log.info("shit", shiieeet);
        StreamSupport.stream(shiieeet.spliterator(), false)
                    .map(RowConverter::convert)
                    .map(this::exportIfSuccessful)
                    .filter(Objects::nonNull)
                    .collect(groupingBy(RowImportResult::getClass));

        return null;
    } catch (Throwable t) {
        log.info("Niebangla", t);
        return null;
    }
}
```

---

## Learning by chaos

Just play with it

```java
public UploadResult importFromFile(File file, String username) {
    try (Workbook workdupa = WorkbookFactory.create(file)) {
        log.info("work", workdupa);
        var shiieeet = workdupa.getSheetAt(0);
        log.info("shit", shiieeet);
        var dupaWtf = 
            StreamSupport.stream(shiieeet.spliterator(), false)
                    .map(RowConverter::convert)
                    .map(this::exportIfSuccessful)
                    .filter(Objects::nonNull)
                    .collect(groupingBy(RowImportResult::getClass));
        log.info("dupa " + dupaWtf);
        return null;
    } catch (Throwable t) {
        log.info("Niebangla", t);
        return null;
    }
}
```

--

Great to learn what can be done.

Get a feeling of how things work.

It sounds stupid. It is not stupid. Just don't commit it into repo. 

---

## Which one is better?

Baby steps

Backward programming

High level design and filling details

Learning by chaos

--

#### Neither. All of these are just to open up your mind, and understand

> what is possible

> how things have to work

> how things can look

> the domain

---

layout: false
background-image: url(img/plotter.gif)
background-size: 70%
background-repeat: no-repeat
background-position: center middle
class: center top

## Some devs think they are a plotter

---

layout: false
background-image: url(img/Digital-Painting-Portrait-01-Process.jpeg)
background-size: 90%
background-repeat: no-repeat
background-position: center middle
class: center top

## It's a process of getting closer to solution

---

layout: false
background-image: url(img/DodWFQ9mQkVyWoKFa0ZIu12PYrPo3P2T0taaK-lgJCo.webp)
background-size: 50%
background-repeat: no-repeat
background-position: center middle
class: center top

## But I have steps... <br /> 1. Fuck around, 2. Refactor

---

layout: false
background-image: url(img/7dd.jpeg)
background-size: 40%
background-repeat: no-repeat
background-position: right center
class: left top

## Expectations

.footnote[https://oktop.tumblr.com/post/15352780846]

---

layout: false
background-image: url(img/horse6.png)
background-size: 50%
background-repeat: no-repeat
background-position: center middle
class: center top

## Reality

---

layout: false
background-image: url(img/process02.png)
background-size: 70%
background-repeat: no-repeat
background-position: center middle
class: center top

## Refactoring is important...

.footnote[https://pattra.io/2019/11/24-figure-drawing/]


---

layout: false
background-image: url(img/process01.png)
background-size: 70%
background-repeat: no-repeat
background-position: center middle
class: center top

## ...creating several drafts is more important

.footnote[https://pattra.io/2019/11/24-figure-drawing/]

---

layout: false
background-image: url(img/bob-ross-promojpg.jpeg)
background-size: 70%
background-repeat: no-repeat
background-position: center middle
class: center top

--

## Every time you see live coding, <br/> there were many drafts before

???

Bob Ross always painted at least once before live

Ask Starbuxman how many times he implemented a live demo

---

template: inverse
background-image: url(img/hemingway.jpeg)
background-size: 120%
background-repeat: no-repeat
background-position: center middle
class: top right

.right-column[

#### "Don’t get discouraged because there’s a lot of mechanical work to writing. There is, and you can’t get out of it. I rewrote the first part of A Farewell to Arms at least fifty times. You’ve got to work it over. 

# The first draft of anything is shit." 
]

.footnote[1984, With Hemingway: A Year in Key West and Cuba by Arnold Samuelson]


---

## Best practices in coding

Fuck around for a while, explore possible solutions

--

Implement it

--

Delete it

--

Implement it again

--

Get closer, repeat

--

Do not be afraid of wasting time!

--

You are not a printer nor plotter, you learn by making mistakes

--

#### Rewriting an implementation when you know what you want is fast!

#### And most of the time cost is in READING code and thinking, not writing

--

When you are good at it, you stop being attached to your code

And that's egoless programming, the best programming there is


---

## Are we there yet?

---

layout: false
background-image: url(img/obsuniv.png)
background-size: 100%
background-repeat: no-repeat
background-position: center middle
class: top right

## All of the the observable universe <br /> on a logarithmic scale 

46.5 billion light-years radius

???

This is all we can observe because light (which is quite fast) hasn't yet reached us from beyond.

As the universe's expansion is accelerating, all currently observable objects, outside our local supercluster, will eventually appear to freeze in time, while emitting progressively redder and fainter light.

And unfortunately, our app is beyound that frontier.

---

layout: false
background-image: url(img/whatisitdoing.jpeg)
background-size: 85%
background-repeat: no-repeat
background-position: center middle
class: top left

## Alas our app is beyond observable universe

---

## What now happens is a dark matter

```java
private void sendToReporterIfAnySuccessful(UploadResult result) {
    if (result.successfullyImported() > 0) {
        reporterFacade.report(result);
    } 
}
```

--

You will never understand, why some imports are not listed

---

## Fiat lux

```java
private void sendToReporterIfAnySuccessful(UploadResult result) {
    if (result.successfullyImported() > 0) {
        reporterFacade.report(result);
    } else {
        log.info("Not sending manual import result for file {} to reporter, 
            because it had zero successfully imported rows", result.fileName());
    }
}
```

--

Good practice after the code is written: add observability

Make sure you have tracing & tech metrics

Create business metrics 

> number of uploads

> distribution of xlsx file sizes in time

> total number of invoice import failures and successes

> number of failures per failure type

> more...


---

layout: false
background-image: url(img/businessmetrics.png)
background-size: 30%
background-repeat: no-repeat
background-position: bottom right
class: top left

## Fiat lux

```java
private void sendToReporterIfAnySuccessful(UploadResult result) {
    if (result.successfullyImported() > 0) {
        reporterFacade.report(result);
    } else {
        log.info("Not sending manual import result for file {} to reporter, 
            because it had zero successfully imported rows", result.fileName());
    }
}
```

Good practice after the code is written: add observability

Make sure you have tracing & tech metrics

Create business metrics 

> number of uploads

> distribution of xlsx file sizes in time

> total number of invoice import failures and successes

> number of failures per failure type

> more...

---

## Fiat lux

```java
private void sendToReporterIfAnySuccessful(UploadResult result) {
    if (result.successfullyImported() > 0) {
        reporterFacade.report(result);
    } else {
        log.info("Not sending manual import result for file {} to reporter, 
            because it had zero successfully imported rows", result.fileName());
    }
}
```
#### But most of all, log every decision branch

#### Every if-else, every Optional.ofNullable, every Mono.empty, every kotlin?.let especially when you don't do anything

Main flow you can skip, though I'd add business logs on entry points to modules, kafka listeners, etc.

Sergei Egorov (atomicjar/testcontainers/groovy/reactor) starts a new microservice designing observability first

---

## Logs should not clutter your code

Use higher order functions to remove defensive programming clutter and dead branches logging from main flow

---

```Java
withLoggedUser(rentDto, (UserDto loggedUser) ->
    withMovieDetails(rentDto, (MovieDto movieDto) -> {
        CostCalculationRequestDto costRequest = 
            costRequestCreator.create(rentDto, movieDto);
        CostDto cost = costCalculatorFacade.calculate(costRequest);
        RentResponseDto response = new RentResponseDto(cost.value());
        MovieWasRentedEvent event = 
            eventCreator.create(rentDto, loggedUser, movieDto, cost);
        eventPublisher.publishEvent(event);
        return response;
    })
);
```

--

```Java
private RentResponseDto withLoggedUser(
        RentRequestDto rentDto, Function<UserDto, RentResponseDto> action) {
    UserDto loggedUser = authenticationFacade.getLoggedUser();
    if(loggedUser == null) {
        log.warn("Lend was called, but user is not logged. RentDto {}", rentDto);
        throw new UserNotFoundForLendException(rentDto);
    }
    return action.apply(loggedUser);
}

private RentResponseDto withMovieDetails(
        RentRequestDto dto, Function<MovieDto, RentResponseDto> action) {
    return listingFacade.showDetails(dto.title())
            .map(action::apply)
            .orElseThrow(() -> {
                log.warn("Film details not found for request {}", dto);
                return new DetailsNotFoundException();
            });
}
```

---

## Logs shoud tell everything (no debugger required)

If when running tests you don't need to turn on debugger, because log explain everything, you are safe in prod.

If you cannot reason on why tests do not work based on logs alone, you won't be able to reason on what's up on prod.

```java
Received a file for manual CDR import. Filename: eON-CDRs-Example-2-rows.xlsx, User: Maciek
Converting row 0 to manual import event
First cell at row 0 recognized as a header. Skipping
Converting row 1 to manual import event
Row 1 converted to values [7d994832-c756-4148-9813-d9a856348e36, 2022-01-01T12:12:49Z, 2022-01-01T12:35:25Z, 0,0156944444, 4,1, Vattenfall - Willys - Marketenterivägen 25, 35236, Växjö, SWE, 3, 347A1485, SE-EDR-C01441446-L, GARO-M1126611-1_2, eOn (SE), InCharge SE, 0, #N/A]
After row conversion, sending event to cdr importer facade. Row convertion result RowConvertionSuccess[rowNumber=1, rowValues=[7d994832-c756-4148-9813-d9a856348e36, 2022-01-01T12:12:49Z, 2022-01-01T12:35:25Z, 0,0156944444, 4,1, Vattenfall - Willys - Marketenterivägen 25, 35236, Växjö, SWE, 3, 347A1485, SE-EDR-C01441446-L, GARO-M1126611-1_2, eOn (SE), InCharge SE, 0, #N/A], event=RoamingCdrImportedEvent(id=null, occurrenceTime=null, startTime=null, durationInSeconds=null, consumptionInKWH=null, stationName=null, pointName=null, lcpName=null, cpoName=null, stationLocation=null, cardNumber=null, rfid=null, cardOwner=null, cdrId=7d994832-c756-4148-9813-d9a856348e36, billingData=null, salesTenant=null, secondGenGroupName=null, autostart=null, chargingPointType=null, sessionId=null, transactionId=null, fullname=null, customerEmspName=null, createDate=null, systemDate=null)]
Converting row 2 to manual import event
Row 2 converted to values [2fc26df3-1f92-4cee-8d4a-9bb6f62b2c29, 2022-01-02T10:10:08Z, 2022-01-02T12:35:28Z, 0,1009259259, 8,5, Vattenfall - Tyresö Centrum - Dalgränd 9, 13540, Tyresö, SWE, 3, 5608E08C, SE-EDR-C00363318-L, GARO-M1126621-1_2, eOn (SE), InCharge SE, 22,1, #N/A]
After row conversion, sending event to cdr importer facade. Row convertion result RowConvertionSuccess[rowNumber=2, rowValues=[2fc26df3-1f92-4cee-8d4a-9bb6f62b2c29, 2022-01-02T10:10:08Z, 2022-01-02T12:35:28Z, 0,1009259259, 8,5, Vattenfall - Tyresö Centrum - Dalgränd 9, 13540, Tyresö, SWE, 3, 5608E08C, SE-EDR-C00363318-L, GARO-M1126621-1_2, eOn (SE), InCharge SE, 22,1, #N/A], event=RoamingCdrImportedEvent(id=null, occurrenceTime=null, startTime=null, durationInSeconds=null, consumptionInKWH=null, stationName=null, pointName=null, lcpName=null, cpoName=null, stationLocation=null, cardNumber=null, rfid=null, cardOwner=null, cdrId=2fc26df3-1f92-4cee-8d4a-9bb6f62b2c29, billingData=null, salesTenant=null, secondGenGroupName=null, autostart=null, chargingPointType=null, sessionId=null, transactionId=null, fullname=null, customerEmspName=null, createDate=null, systemDate=null)]
Manual import finished with result SuccessfulManualImportResult[fileName=eON-CDRs-Example-2-rows.xlsx, userName=Maciek, successfullyImported=2]
```

---

```java
SpockReportExtension - Got configuration from Spock: com.athaydes.spockframework.report.internal.SpockReportsConfiguration([:])
SpockReportExtension - Configuring com.athaydes.spockframework.report.SpockReportExtension
ConfigLoader - SpockReports config loaded: {com.athaydes.spockframework.report.projectVersion=Unknown, com.athaydes.spockframework.report.aggregatedJsonReportDir=, com.athaydes.spockframework.report.template.TemplateReportCreator.enabled=true, com.athaydes.spockframework.report.internal.HtmlReportCreator.printThrowableStackTrace=false, com.athaydes.spockframework.report.template.TemplateReportCreator.specTemplateFile=/templateReportCreator/spec-template.md, com.athaydes.spockframework.report.template.TemplateReportCreator.reportFileExtension=md, com.athaydes.spockframework.report.hideEmptyBlocks=false, com.athaydes.spockframework.report.template.TemplateReportCreator.summaryTemplateFile=/templateReportCreator/summary-template.md, com.athaydes.spockframework.report.template.TemplateReportCreator.summaryFileName=summary.md, com.athaydes.spockframework.report.internal.HtmlReportCreator.enabled=true, com.athaydes.spockframework.report.outputDir=build/spock-reports, com.athaydes.spockframework.report.projectName=, com.athaydes.spockframework.report.showCodeBlocks=false, com.athaydes.spockframework.report.internal.HtmlReportCreator.specSummaryNameOption=class_name_and_title, com.athaydes.spockframework.report.internal.HtmlReportCreator.inlineCss=true, com.athaydes.spockframework.report.internal.HtmlReportCreator.featureReportCss=spock-feature-report.css, com.athaydes.spockframework.report.testSourceRoots=src/test/groovy, com.athaydes.spockframework.report.internal.HtmlReportCreator.summaryReportCss=spock-summary-report.css, com.athaydes.spockframework.report.internal.HtmlReportCreator.excludeToc=false, com.athaydes.spockframework.report.IReportCreator=com.athaydes.spockframework.report.internal.HtmlReportCreator}
ConfigLoader - Property projectVersion set to Unknown
ConfigLoader - Property aggregatedJsonReportDir set to 
ConfigLoader - Ignoring property 'enabled' for IReportCreator of incompatible type com.athaydes.spockframework.report.internal.HtmlReportCreator
ConfigLoader - Property printThrowableStackTrace set to false
ConfigLoader - Ignoring property 'specTemplateFile' for IReportCreator of incompatible type com.athaydes.spockframework.report.internal.HtmlReportCreator
ConfigLoader - Ignoring property 'reportFileExtension' for IReportCreator of incompatible type com.athaydes.spockframework.report.internal.HtmlReportCreator
ConfigLoader - Property hideEmptyBlocks set to false
ConfigLoader - Ignoring property 'summaryTemplateFile' for IReportCreator of incompatible type com.athaydes.spockframework.report.internal.HtmlReportCreator
ConfigLoader - Ignoring property 'summaryFileName' for IReportCreator of incompatible type com.athaydes.spockframework.report.internal.HtmlReportCreator
ConfigLoader - Property enabled set to true
ConfigLoader - Property outputDir set to build/spock-reports
ConfigLoader - Property projectName set to 
ConfigLoader - Property showCodeBlocks set to false
ConfigLoader - Property specSummaryNameOption set to class_name_and_title
ConfigLoader - Property inlineCss set to true
ConfigLoader - Property featureReportCss set to spock-feature-report.css
ConfigLoader - Property testSourceRoots set to src/test/groovy
ConfigLoader - Property summaryReportCss set to spock-summary-report.css
ConfigLoader - Property excludeToc set to false
SpecInfoListener - Before spec: ManualCdrImporterSpec
SpecInfoListener - Before feature: should convert each line except header into RoamingCdrImportedEvent and pass to importer
SpecInfoListener - Before iteration: should convert each line except header into RoamingCdrImportedEvent and pass to importer
ManualImporterFacade - Received a file for manual CDR import. Filename: eON-CDRs-Example-2-rows.xlsx, User: Maciek
PackageRelationshipCollection - Parsing relationship: /xl/_rels/workbook.xml.rels
PackageRelationshipCollection - Parsing relationship: /xl/worksheets/_rels/sheet1.xml.rels
PackageRelationshipCollection - Parsing relationship: /_rels/.rels
POIXMLFactory - using default POIXMLDocumentPart for http://schemas.openxmlformats.org/officeDocument/2006/relationships/printerSettings
```

---

```Java
RowConverter - Converting row 0 to manual import event
RowConverter - First cell at row 0 recognized as a header. Skipping
RowConverter - Converting row 1 to manual import event
RowConverter - Row 1 converted to values [7d994832-c756-4148-9813-d9a856348e36, 2022-01-01T12:12:49Z, 2022-01-01T12:35:25Z, 0,0156944444, 4,1, Willys - Marketenterivägen 25, 35236, Växjö, SWE, 3, 347A1485, SE-EDR-C01441446-L, GARO-M1126611-1_2, eOn (SE), InCharge SE, 0, #N/A]
WorkbookProcessor - After row conversion, sending event to cdr importer facade. Row convertion result RowConvertionSuccess[rowNumber=1, rowValues=[7d994832-c756-4148-9813-d9a856348e36, 2022-01-01T12:12:49Z, 2022-01-01T12:35:25Z, 0,0156944444, 4,1, Willys - Marketenterivägen 25, 35236, Växjö, SWE, 3, 347A1485, SE-EDR-C01441446-L, GARO-M1126611-1_2, eOn (SE), InCharge SE, 0, #N/A], event=RoamingCdrImportedEvent(id=null, occurrenceTime=null, startTime=null, durationInSeconds=null, consumptionInKWH=null, stationName=null, pointName=null, lcpName=null, cpoName=null, stationLocation=null, cardNumber=null, rfid=null, cardOwner=null, cdrId=7d994832-c756-4148-9813-d9a856348e36, billingData=null, salesTenant=null, secondGenGroupName=null, autostart=null, chargingPointType=null, sessionId=null, transactionId=null, fullname=null, customerEmspName=null, createDate=null, systemDate=null)]
RowConverter - Converting row 2 to manual import event
RowConverter - Row 2 converted to values [2fc26df3-1f92-4cee-8d4a-9bb6f62b2c29, 2022-01-02T10:10:08Z, 2022-01-02T12:35:28Z, 0,1009259259, 8,5, Tyresö Centrum - Dalgränd 9, 13540, Tyresö, SWE, 3, 5608E08C, SE-EDR-C00363318-L, GARO-M1126621-1_2, eOn (SE), InCharge SE, 22,1, #N/A]
WorkbookProcessor - After row conversion, sending event to cdr importer facade. Row convertion result RowConvertionSuccess[rowNumber=2, rowValues=[2fc26df3-1f92-4cee-8d4a-9bb6f62b2c29, 2022-01-02T10:10:08Z, 2022-01-02T12:35:28Z, 0,1009259259, 8,5, Tyresö Centrum - Dalgränd 9, 13540, Tyresö, SWE, 3, 5608E08C, SE-EDR-C00363318-L, GARO-M1126621-1_2, eOn (SE), InCharge SE, 22,1, #N/A], event=RoamingCdrImportedEvent(id=null, occurrenceTime=null, startTime=null, durationInSeconds=null, consumptionInKWH=null, stationName=null, pointName=null, lcpName=null, cpoName=null, stationLocation=null, cardNumber=null, rfid=null, cardOwner=null, cdrId=2fc26df3-1f92-4cee-8d4a-9bb6f62b2c29, billingData=null, salesTenant=null, secondGenGroupName=null, autostart=null, chargingPointType=null, sessionId=null, transactionId=null, fullname=null, customerEmspName=null, createDate=null, systemDate=null)]
ZipPackage - Save content types part
ZipPackage - Save package relationships
ZipPackage - Save part 'docProps/app.xml'
ZipPackage - Save part 'docProps/core.xml'
ZipPackage - Save part 'docProps/custom.xml'
ZipPackage - Save part 'xl/printerSettings/printerSettings1.bin'
ZipPackage - Save part 'xl/sharedStrings.xml'
ZipPackage - Save part 'xl/styles.xml'
ZipPackage - Save part 'xl/theme/theme1.xml'
ZipPackage - Save part 'xl/workbook.xml'
ZipPackage - Save part 'xl/worksheets/sheet1.xml'
ManualImporterFacade - Manual import finished with result SuccessfulManualImportResult[fileName=eON-CDRs-Example-2-rows.xlsx, userName=Maciek, successfullyImported=2]
SpecInfoListener - After iteration: should convert each line except header into RoamingCdrImportedEvent and pass to importer
SpecInfoListener - After feature: should convert each line except header into RoamingCdrImportedEvent and pass to importer
SpecInfoListener - After spec: ManualCdrImporterSpec
CssResource - Getting classpath resource text: spock-feature-report.css
CssResource - Found css resource (2987 characters long)
CssResource - Inlining css in HTML report
CssResource - Getting classpath resource text: spock-summary-report.css
CssResource - Found css resource (887 characters long)
CssResource - Inlining css in HTML report
```

---

In Spring Boot, you need to add 

```
test/resources/logback-test.xml
```

And setup everything

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!-- WARNING: this file overrides all spring configurations -->
<configuration
        xmlns="http://ch.qos.logback/xml/ns/logback"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://ch.qos.logback/xml/ns/logback https://raw.githubusercontent.com/enricopulatzo/logback-XSD/master/src/main/xsd/logback.xsd http://ch.qos.logback/xml/ns/logback ">

    <!-- Console patterns and conversions are 1:1 with Spring default logback config-->
    <conversionRule conversionWord="clr" 
        converterClass="org.springframework.boot.logging.logback.ColorConverter" />
    <conversionRule conversionWord="wex" 
        converterClass="org.springframework.boot.logging.logback.WhitespaceThrowableProxyConverter" />
    <conversionRule conversionWord="wEx" 
        converterClass="org.springframework.boot.logging.logback.ExtendedWhitespaceThrowableProxyConverter" />

    <property name="CONSOLE_LOG_PATTERN" 
        value="${CONSOLE_LOG_PATTERN:-%clr(%d{${LOG_DATEFORMAT_PATTERN:-yyyy-MM-dd HH:mm:ss.SSS}}){faint} %clr(${LOG_LEVEL_PATTERN:-%5p}) %clr(${PID:- }){magenta} %clr(---){faint} %clr([%15.15t]){faint} %clr(%-40.40logger{39}){cyan} %clr(:){faint} %m%n${LOG_EXCEPTION_CONVERSION_WORD:-%wEx}}"/>

    <appender name="STDOUT" class="ch.qos.logback.core.ConsoleAppender">
        <encoder>
            <pattern>${CONSOLE_LOG_PATTERN}</pattern>
        </encoder>
    </appender>

    <root level="info">
        <appender-ref ref="STDOUT"/>
    </root>
    ...
```

---

```xml
...

<!-- This is what we really want to see in our test logs -->
<logger name="dev.nabrdalik" level="DEBUG"/>
<!-- Apache POI - we might want to look at it -->
<logger name="org.apache.poi.openxml4j.opc" level="INFO"/>
<!-- Spock - we don't want spock talking to us too much -->
<logger name="com.athaydes.spockframework.report" level="WARN"/>
<!-- This is used even in unit tests, very important to turn off spamming log -->
<logger name="org.hibernate.validator.internal.util.Version" level="WARN"/>
```

--

```xml
<!-- Tunning logs for integration tests -->
<logger name="kafka" level="ERROR"/>
<logger name="org.apache.kafka" level="ERROR"/>
<logger name="org.apache.zookeeper.server" level="OFF"/>
<logger name="level.org.springframework" level="INFO"/>
<logger name="level.reactor.netty.http.client" level="INFO"/>
<logger name="org.hibernate" level="INFO" />
<logger name="org.apache.catalina.startup.DigesterFactory" level="ERROR"/>
<logger name="org.apache.catalina.util.LifecycleBase" level="ERROR"/>
<logger name="org.apache.coyote.http11.Http11NioProtocol" level="WARN"/>
<logger name="org.apache.sshd.common.util.SecurityUtils" level="WARN"/>
<logger name="org.apache.tomcat.util.net.NioSelectorPool" level="WARN"/>
<logger name="org.eclipse.jetty.util.component.AbstractLifeCycle" level="ERROR"/>
<logger name="org.springframework.kafka.listener.KafkaMessageListenerContainer" level="ERROR"/>
<logger name="org.apache.kafka.clients.consumer.ConsumerConfig" level="ERROR"/>
...
```
---

layout: false
background-image: url(img/IMG_20220215_122110.jpeg)
background-size: 45%
background-repeat: no-repeat
background-position: center right

.left-column[
## Mob code-review

Large reviews are boring or hard

So don't just do async reviews on a PRs

Do a sync review after standup and explain what you've learned

Share knowledge!

If you do it while everyone is sipping coffee, it's cost effective

]

---

layout: false
background-image: url(img/elden2.webp)
background-size: 45%
background-repeat: no-repeat
background-position: center right

.left-column[

# How not to fix bugs in production

What's the worst thing we can do?

]

???

Users report that some invoices appear twice in billing

Put pressure on people that they have to be fast, we are losing money, no time to waste, start working on the first idea they have.

After all we have 7 people in the team

---


layout: false
background-image: url(img/elden2.webp)
background-size: 45%
background-repeat: no-repeat
background-position: center right

.left-column[

# How not to fix bugs in production

What's the worst thing we can do?

Rush to fix it right away!

> We are losing money!

> No time to waste!

]

???


Put pressure on people that they have to be fast, we are losing money, no time to waste, start working on the first idea they have.

After all we have 7 people in the team

---


layout: false
background-image: url(img/elden2.webp)
background-size: 45%
background-repeat: no-repeat
background-position: center right

.left-column[

# How not to fix bugs in production

What's the worst thing we can do?

Rush to fix it right away!

> We are losing money!

> No time to waste!

Event worse?

> All hands on board!

> Implement ALL fix ideas concurrently right away

]

???


Put pressure on people that they have to be fast, we are losing money, no time to waste, start working on the first idea they have.

After all we have 7 people in the team

---

# Why you don't want to rush

Urgency == implement the first fix that comes to your mind

???

Push hard == first thing that comes to your mind

--

Stressed people == buggy fix

???

Stressed people introduce a lot of bugs

--

First idea is usually wrong

???

First idea is usually wrong

--

Second fix => original bug + bugs from first fix

???

Second will have to deal with original bug and the new bugs introduced by first idea

--

N Concurrent fixes == N! interference, N! bugs

???

If you get several "solutions" to the prod at the same time with little review, you'll have many more bugs that complicate the situation even more.

Wait, it even has a name

---

layout: false
background-image: url(img/shotgun.png)
background-size: 60%
background-repeat: no-repeat
background-position: center bottom


# Shotgun debugging

> A process of making relatively un-directed changes to software in the hope that a bug will be perturbed out of existence.

> Using the approach of trying several possible solutions of hardware or software problem at the same time, in the hope that one of the solutions (typically source code modifications) will work 

> [https://en.wikipedia.org/wiki/Shotgun_debugging]

???

You may have "solved the problem" by a combination of workarounds and fixes, still without understanding what the problem actually was

---

# What's the correct approach?

Get everyone, form hypotheses, do not fix

???

Get your team together. Write down all the ideas of what the bug might be. These are your hypotheses. 

Do not try to fix it yet.

---

layout: false
background-image: url(img/wednesdaymydudeswide.jpeg)
background-size: 60%
background-repeat: no-repeat
background-position: right center


# What's the correct approach?

Get everyone, form hypotheses, do not fix

1. file was uploaded twice
2. xlsx had two identical rows
3. bug in importer
4. we duplicate a row on parsing
5. we calculate outcomes wrong

--

For each: create a verification plan

Confirm or reject (Rejection is easier!)

???

For each hypothesis, come up with a verification plan - how to confirm or reject the hypothesis the fastest. Focus on rejection - it's usually much easier to do.

--

Cluster (few min/longer) and eliminate (in parallel)

???

Now reject/confirm those till only one is left. Start either with the fastest to confirm/reject to narrow your options or with the most probable. I usually cluster the solutions into "We can verify this right away" and "This will take some time to verify". If you are left with two. Try to come up with new verification plans.


--

Once you are left with only a few, NOW try to fix

???

Only after you are able to confirm a hypothesis, work on a fix. And never deploy two at once, rather roll-back the first fix if it didn't work, and apply a new one.

Oh, that approach also has a name

---

# Scientific method

Benefits

> you'll solve a bug much faster (on average), because you do essentially a breadth-first search

> you will introduce less changes, and therefore less new bugs to the system

> while verifying hypotheses you will understand the system better

---

#### Before the code

Prove the validity: stop writing useless code

Design the architecture; Design for failure (use threat modelling)

Split solution into MVPs (early feedback is crucial)

Refine requirements as Behavioral Scenarios for each module (analysis)
<br /> Use tests to manage requirements, use them to communicate with business

#### Writing the code

Explore possible solutions (fuck around for a while)
<br /> Implement a few drafts (not fully)

Implement and refactor final version when you see it in your mind

Add observability (business metrics & logs on each code branch)
<br /> Do not use debugger - when logs tell you everything in TDD/BDD, you are good

#### After the code

Use mob review for large PRs, fastest way to share knowledge

Use scientific method: form hypotheses as a team, eliminate them

---

class: center middle


# Good luck

More on architecture: What I wish I knew when I started designing systems <br />
https://www.youtube.com/watch?v=1HJJhGHC2A4

More on testing: Improving your Test Driven Development in 45 minutes <br />https://www.youtube.com/watch?v=2vEoL3Irgiw

Twitter @jnabrdalik

jakubn@gmail.com 

https://nabrdalik.dev/

If you want your team to learn:
https://bottega.com.pl/

This presentation is available at <br />
https://jakubn.gitlab.io/bestpracticesinpractice




